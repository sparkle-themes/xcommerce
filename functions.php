<?php
/**
 * Describe child theme functions
 *
 * @package Spark Multipurpose
 * @subpackage xCommerce
 * 
 */

 if ( ! function_exists( 'xcommerce_setup' ) ) :

    
    /**
     * Sets up theme defaults and registers support for various WordPress features.
     *
     * Note that this function is hooked into the after_setup_theme hook, which
     * runs before the init hook. The init hook is too late for some features, such
     * as indicating support for post thumbnails.
     */
    function xcommerce_setup() {

        /*
        * Make theme available for translation.
        * Translations can be filed in the /languages/ directory.
        * If you're building a theme based on xCommerce, use a find and replace
        * to change 'xcommerce' to the name of your theme in all the template files.
        */
        load_theme_textdomain( 'xcommerce', get_template_directory() . '/languages' );

        add_theme_support( "title-tag" );
        add_theme_support( 'automatic-feed-links' );

        $xcommerce_theme_info = wp_get_theme();
        $GLOBALS['xcommerce_version'] = $xcommerce_theme_info->get( 'Version' );
    }
endif;
add_action( 'after_setup_theme', 'xcommerce_setup' );


/**
 * Enqueue child theme styles and scripts
*/
function xcommerce_scripts() {
    
    global $xcommerce_version;
    wp_dequeue_style( 'spark-multipurpose-responsive' );
    wp_enqueue_style( 'spark-multipurpose-parent-style', trailingslashit( esc_url ( get_template_directory_uri() ) ) . 'style.css', array(), esc_attr( $xcommerce_version ) );
    wp_enqueue_style('spark-multipurpose-responsive');
    
    wp_enqueue_script('custom', get_stylesheet_directory_uri() . '/js/custom.js', array('jquery'), true);
}
add_action( 'wp_enqueue_scripts', 'xcommerce_scripts', 20 );


if ( ! function_exists( 'xcommerce_child_options' ) ) {

    function xcommerce_child_options( $wp_customize ) {

         if (class_exists('woocommerce')) {
            require get_stylesheet_directory() . '/inc/customizer/product-type-settings.php';
            require get_stylesheet_directory() . '/inc/customizer/product-category-settings.php';
            require get_stylesheet_directory() . '/inc/customizer/product-hotoffer.php';
        }
        
        
        /** header call button and icon */
        $wp_customize->add_setting('xcommerce-show-cart', array(
            'default' => 'enable',
            'sanitize_callback' => 'xcommerce_sanitize_switch',	
        ));

        $wp_customize->add_control(new Spark_Multipurpose_Switch_Control($wp_customize, 'xcommerce-show-cart', array(
            'label' => esc_html__('Cart Icon', 'xcommerce'),
            'section' => 'xcommerce_header_settings',
            'switch_label' => array(
                'enable' => esc_html__('Yes', 'xcommerce'),
                'disable' => esc_html__('No', 'xcommerce'),
            ),
        )));

        $wp_customize->add_setting( 'xcommerce-cart-icon',
            array(
                'default'           => 'fas fa-cart-arrow-down',
                'sanitize_callback' => 'sanitize_text_field',
            )
        );

        // $wp_customize->add_control( new Spark_Multipurpose_Fontawesome_Icons($wp_customize, 'xcommerce-cart-icon',
        //     array(
        //         'label'    => esc_html__( 'Cart Icon', 'xcommerce' ),
        //         'section'  => 'xcommerce_header_settings',
        //         'settings' => 'xcommerce-cart-icon',
        //         'type'     => 'icon',
        //     )
        // ));


        /****
         * Advance Promo Feature Services
         */
        $wp_customize->get_setting('spark_multipurpose_promoservice_type')->priority = 11;
        $id = "promoservice";
        $wp_customize->add_setting("spark_multipurpose_{$id}_advance_settings", array(
            // 'transport' => 'postMessage',
            'sanitize_callback' => 'spark_multipurpose_sanitize_repeater',		//done
            'priority' => 12,
            'default' => json_encode(array(
                array(
                    'block_image'      => '',
                    'block_icon'       => 'fas fa-address-card',
                    'block_title'      => '',
                    'block_desc'       => '',
                    'button_text'      => '',
                    'button_url'       => '',
                    'block_bg_color'   => '',
                    'block_color'      => '',
                    'block_alignment'  => '',
                )
            ))
        ));
        $wp_customize->add_control(new Spark_Multipurpose_Repeater_Control( $wp_customize, "spark_multipurpose_{$id}_advance_settings", 
            array(
                'label' 	   => esc_html__('Promo Service Settings', 'spark-multipurpose'),
                'section' 	   => "spark_multipurpose_{$id}_section",
                'settings' 	   => "spark_multipurpose_{$id}_advance_settings",
                'box_label' => esc_html__('Service Block Item', 'spark-multipurpose'),
                'add_label' => esc_html__('Add New Item', 'spark-multipurpose'),
            ),
            array(
                'block_image' => array(
                    'type' => 'upload',
                    'label' => __("Upload Image", 'spark-multipurpose'),
                ),
                'block_icon' => array(
                    'type' => 'icon',
                    'label' => esc_html__('Choose Icon', 'spark-multipurpose'),
                    'default' => 'fas fa-address-card'
                ),
                'block_title' => array(
                    'type' => 'text',
                    'label' => __("Title", 'spark-multipurpose'),
                ),
                'block_desc' => array(
                    'type' => 'textarea',
                    'label' => __("Short Description", 'spark-multipurpose'),
                ),
                
                'button_text' => array(
                    'type' => 'text',
                    'label' => esc_html__('Enter First Button Text', 'spark-multipurpose'),
                    'default' => ''
                ),
                'button_url' => array(
                    'type' => 'url',
                    'label' => esc_html__('Enter First Button Url', 'spark-multipurpose'),
                    'default' => ''
                ),
            )
        ));


        $wp_customize->add_setting("spark_multipurpose_catelog_sidebar", array(
            'default' => 'no',
            'sanitize_callback' => 'spark_multipurpose_sanitize_select',
            // 'transport' => 'postMessage'
        ));
        $wp_customize->add_control("spark_multipurpose_catelog_sidebar", array(
            'section' => "woocommerce_product_catalog",
            'type' => 'select',
            'label' => esc_html__('Sidebar', 'spark-multipurpose'),
            'choices' => array(
                'no-center-content'      => esc_html__('Default', 'spark-multipurpose'),
                'left'  => esc_html__('Left', 'spark-multipurpose'),
                'right' => esc_html__('Right', 'spark-multipurpose')
            )
        ));



    }
}
add_action( 'customize_register' , 'xcommerce_child_options', 11 );



/**
 * Enqueue required scripts/styles for customizer panel
 *
 * @since 1.0.0
 *
 */
function xcommerce_customize_scripts(){
	wp_enqueue_script('xcommerce-customizer', get_stylesheet_directory_uri() . '/js/admin.js', array('jquery', 'customize-controls'), true);
}
add_action('customize_controls_enqueue_scripts', 'xcommerce_customize_scripts');


/**
 * Load WooCommerce compatibility file.
 */
if ( class_exists( 'WooCommerce' ) ) {
	require get_stylesheet_directory() . '/inc/woocommerce.php';
}

if( !function_exists('xcommerce_woocommerce_category')){
	function xcommerce_woocommerce_category(){
		$taxonomy     = 'product_cat';
		$empty        = 1;
		$orderby      = 'name';  
		$show_count   = 0;      // 1 for yes, 0 for no
		$pad_counts   = 0;      // 1 for yes, 0 for no
		$hierarchical = 1;      // 1 for yes, 0 for no  
		$title        = '';  
		$empty        = 0;
		$args = array(
			'taxonomy'     => $taxonomy,
			'orderby'      => $orderby,
			'show_count'   => $show_count,
			'pad_counts'   => $pad_counts,
			'hierarchical' => $hierarchical,
			'title_li'     => $title,
			'hide_empty'   => $empty
		);
		$woocommerce_categories = array();
		$woocommerce_categories_obj = get_categories( $args );
		foreach( $woocommerce_categories_obj as $category ) {
			$woocommerce_categories[$category->term_id] = $category->name;
		}
		return $woocommerce_categories;
	}
}