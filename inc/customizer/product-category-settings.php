<?php
/**
 * Product Type Settings
*/
$wp_customize->add_section(new Spark_Multipurpose_Toggle_Section($wp_customize, 'spark_multipurpose_productcat_section', array(
    'title' => esc_html__('Product Category', 'spark-multipurpose'),
    'panel' => 'spark_multipurpose_frontpage_settings',
    'priority' => spark_multipurpose_get_section_position('spark_multipurpose_productcat_section'),
    'hiding_control' => 'spark_multipurpose_productcat_section_disable'
)));


/**
 * Enable/Disable Option
 *
 * @since 1.0.0
*/
$wp_customize->add_setting('spark_multipurpose_productcat_section_disable', array(
    'default' => 'disable',
    'transport' => 'postMessage',
    'sanitize_callback' => 'spark_multipurpose_sanitize_switch',     //done
));
$wp_customize->add_control(new Spark_Multipurpose_Switch_Control($wp_customize, 'spark_multipurpose_productcat_section_disable', array(
    'label' => esc_html__('Enable', 'spark-multipurpose'),
    'section' => 'spark_multipurpose_productcat_section',
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'spark-multipurpose'),
        'disable' => esc_html__('No', 'spark-multipurpose'),
    ),
)));

$wp_customize->add_setting('spark_multipurpose_productcat_nav', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));
$wp_customize->add_control(new Spark_Multipurpose_Custom_Control_Tab($wp_customize, 'spark_multipurpose_productcat_nav', array(
    'type' => 'tab',
    'section' => 'spark_multipurpose_productcat_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Content', 'spark-multipurpose'),
            'fields' => array(
                'spark_multipurpose_productcat_section_disable',
                'spark_multipurpose_productcat_title_subtitle_heading',
                'spark_multipurpose_productcat_super_title',
                'spark_multipurpose_productcat_title',
                'spark_multipurpose_productcat_sub_title',
                'spark_multipurpose_productcat_title_align',
                'spark_multipurpose_productcat_content',
                'spark_multipurpose_productcat_image',
                'spark_multipurpose_productcat_image_position',
                'spark_multipurpose_productcat_category_view',
                'spark_multipurpose_productcat_category',
                'spark_multipurpose_productcat_layout',
                'spark_multipurpose_productcat_column',
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Style', 'spark-multipurpose'),
            'fields' => array(
                'spark_multipurpose_productcat_cs_heading',
                'spark_multipurpose_productcat_super_title_color',
                'spark_multipurpose_productcat_title_color',
            ),
        ),
        array(
            'name' => esc_html__( 'Advance', 'spark-multipurpose'),
            'fields' => array(
                'spark_multipurpose_productcat_bg_type',
                'spark_multipurpose_productcat_bg_color',
                'spark_multipurpose_productcat_bg_gradient',
                'spark_multipurpose_productcat_bg_image',
                'spark_multipurpose_productcat_bg_video',
                'spark_multipurpose_productcat_overlay_color',

                'spark_multipurpose_productcat_content_heading',
                'spark_multipurpose_productcat_content_bg_type',
                'spark_multipurpose_productcat_content_bg_color',
                'spark_multipurpose_productcat_content_bg_gradient',
                'spark_multipurpose_productcat_content_padding',
                'spark_multipurpose_productcat_content_margin',
                'spark_multipurpose_productcat_content_radius',

                'spark_multipurpose_productcat_padding',
                'spark_multipurpose_productcat_cs_seperator',
                'spark_multipurpose_productcat_seperator0',
                'spark_multipurpose_productcat_section_seperator',
                'spark_multipurpose_productcat_seperator1',
                'spark_multipurpose_productcat_top_seperator',
                'spark_multipurpose_productcat_ts_color',
                'spark_multipurpose_productcat_ts_height',
                'spark_multipurpose_productcat_seperator2',
                'spark_multipurpose_productcat_bottom_seperator',
                'spark_multipurpose_productcat_bs_color',
                'spark_multipurpose_productcat_bs_height'
            )
        ),
        array(
            'name' => esc_html__( 'Hidden', 'spark-multipurpose'),
            'class' => 'hidden customizer-hidden',
            'fields' => array(
                'spark_multipurpose_productcat_text_color',
                'spark_multipurpose_productcat_link_color',
                'spark_multipurpose_productcat_link_hover_color'
            )
        )
    ),
)));

$wp_customize->add_setting('spark_multipurpose_productcat_title_subtitle_heading', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field'
));
$wp_customize->add_control(new Spark_Multipurpose_Customize_Heading($wp_customize, 'spark_multipurpose_productcat_title_subtitle_heading', array(
    'section' => 'spark_multipurpose_productcat_section',
    'label' => esc_html__('Section Title & Sub Title', 'spark-multipurpose')
)));

$wp_customize->add_setting('spark_multipurpose_productcat_super_title', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage'
));
$wp_customize->add_control('spark_multipurpose_productcat_super_title', array(
    'section' => 'spark_multipurpose_productcat_section',
    'type' => 'text',
    'label' => esc_html__('Super Title', 'spark-multipurpose')
));
$wp_customize->add_setting('spark_multipurpose_productcat_title', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage'
));
$wp_customize->add_control('spark_multipurpose_productcat_title', array(
    'section' => 'spark_multipurpose_productcat_section',
    'type' => 'text',
    'label' => esc_html__('Title', 'spark-multipurpose')
));

$wp_customize->add_setting('spark_multipurpose_productcat_title_align', array(
    'default' => 'text-center',
    'sanitize_callback' => 'spark_multipurpose_sanitize_select',
    'transport' => 'postMessage'
));

$wp_customize->add_control(
    new Spark_Multipurpose_Custom_Control_Buttonset( $wp_customize, 'spark_multipurpose_productcat_title_align',
        array(
            'choices'  => array(
                'text-left' => esc_html__('Left', 'spark-multipurpose'),
                'text-center' => esc_html__('Center', 'spark-multipurpose'),
                'text-right' => esc_html__('Right', 'spark-multipurpose'),
            ),
            'label'    => esc_html__( 'Alignment', 'spark-multipurpose' ),
            'section'  => 'spark_multipurpose_productcat_section',
            'settings' => 'spark_multipurpose_productcat_title_align',
        )
    )
);


$wp_customize->add_setting('spark_multipurpose_productcat_category', array(
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage'
));

$wp_customize->add_control(new Spark_Multipurpose_Multiple_Check_Control($wp_customize, 'spark_multipurpose_productcat_category', array(
    'label'     => esc_html__('Select Category', 'spark-multipurpose'),
    'settings' => 'spark_multipurpose_productcat_category',
    'section'  => 'spark_multipurpose_productcat_section',
    'choices'  => xcommerce_woocommerce_category(),
)));

$wp_customize->selective_refresh->add_partial( 'productcat_category', array(
    'settings' => array( 'spark_multipurpose_productcat_category' ),
    'selector' => '#productcat-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'productcat' );
    },
));

$wp_customize->add_setting('spark_multipurpose_productcat_layout', array(
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'category-style-1',
    'transport' => 'postMessage'
));
$wp_customize->add_control('spark_multipurpose_productcat_layout', array(
    'section' => 'spark_multipurpose_productcat_section',
    'type' => 'select',
    'label' => esc_html__('Layout', 'spark-multipurpose'),
    'choices' => array(
        'category-style-1' => __('Style One', 'spark-multipurpose'),
        'category-style-2' => __('Style Two', 'spark-multipurpose'),
        'category-style-3' => __('Style Three', 'spark-multipurpose'),
        'category-style-4' => __('Style Four', 'spark-multipurpose'),
    )
));
$wp_customize->selective_refresh->add_partial( 'spark_multipurpose_productcat_layout', array(
    'selector' => '#productcat-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'productcat' );
    }
));
$wp_customize->add_setting('spark_multipurpose_productcat_category_view', array(
    'default' => 'grid',
    'sanitize_callback' => 'sanitize_text_field',
    'transport' => 'postMessage'
));
$wp_customize->add_control('spark_multipurpose_productcat_category_view', array(
    'section' => 'spark_multipurpose_productcat_section',
    'type' => 'select',
    'label' => esc_html__('View', 'spark-multipurpose'),
    'choices' => array(
        'grid'  => esc_html__('Grid', 'spark-multipurpose'),
        'slider'  => esc_html__('Slider', 'spark-multipurpose')
    )
));
$wp_customize->selective_refresh->add_partial('productcat_category_view', array(
    'settings' => array( 'spark_multipurpose_productcat_category_view' ),
    'selector' => '#productcat-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'productcat' );
    }
));
$wp_customize->add_setting('spark_multipurpose_productcat_column', array(
    'sanitize_callback' => 'absint',
    'default' => 3,
    'transport' => 'postMessage',
));
$wp_customize->add_control(new Spark_Multipurpose_Range_Control($wp_customize, 'spark_multipurpose_productcat_column', array(
    'section' => 'spark_multipurpose_productcat_section',
    'label' => esc_html__('No of Columns', 'spark-multipurpose'),
    'input_attrs' => array(
        'min' => 1,
        'max' => 6,
        'step' => 1,
    )
)));
$wp_customize->selective_refresh->add_partial('productcat_column', array(
    'settings' => array( 
        'spark_multipurpose_productcat_column',
        'spark_multipurpose_productcat_section_seperator', 
        'spark_multipurpose_productcat_top_seperator', 
        'spark_multipurpose_productcat_bottom_seperator'
     ),
    'selector' => '#productcat-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'productcat' );
    }
));

$wp_customize->add_setting('spark_multipurpose_productcat_upgrade_text', array(
    'sanitize_callback' => 'sanitize_text_field'
));

$wp_customize->add_control(new Spark_Multipurpose_Upgrade_Text($wp_customize, 'spark_multipurpose_productcat_upgrade_text', array(
    'section' => 'spark_multipurpose_productcat_section',
    'label' => esc_html__('For more settings,', 'spark-multipurpose'),
    'choices' => array(
        esc_html__('Change title styles', 'spark-multipurpose'),
        esc_html__('Different types of layout', 'spark-multipurpose'),
        esc_html__('Change number of columns', 'spark-multipurpose'),
        esc_html__('Switch between Color/Gradient/Image background', 'spark-multipurpose'),
        esc_html__('Advance Option - Directly Change Content from customizer', 'spark-multipurpose'),
    ),
    'priority' => 400
)));