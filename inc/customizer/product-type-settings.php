<?php
/**
 * Product Type Settings
*/
$wp_customize->add_section(new Spark_Multipurpose_Toggle_Section($wp_customize, 'spark_multipurpose_producttype_section', array(
    'title' => esc_html__('Product Type (Tab)', 'xcommerce'),
    'panel' => 'spark_multipurpose_frontpage_settings',
    'priority' => spark_multipurpose_get_section_position('spark_multipurpose_producttype_section'),
    'hiding_control' => 'spark_multipurpose_producttype_section_disable'
)));

/**
 * Enable/Disable Option
 *
 * @since 1.0.0
*/
$wp_customize->add_setting('spark_multipurpose_producttype_section_disable', array(
    'default' => 'disable',
    'transport' => 'postMessage',
    'sanitize_callback' => 'spark_multipurpose_sanitize_switch',     //done
));
$wp_customize->add_control(new Spark_Multipurpose_Switch_Control($wp_customize, 'spark_multipurpose_producttype_section_disable', array(
    'label' => esc_html__('Enable', 'spark-multipurpose'),
    'section' => 'spark_multipurpose_producttype_section',
    'switch_label' => array(
        'enable' => esc_html__('Yes', 'spark-multipurpose'),
        'disable' => esc_html__('No', 'spark-multipurpose'),
    ),
)));


$wp_customize->add_setting('spark_multipurpose_producttype_nav', array(
    // 'transport' => 'postMessage',
    'sanitize_callback' => 'wp_kses_post',
));
$wp_customize->add_control(new Spark_Multipurpose_Custom_Control_Tab($wp_customize, 'spark_multipurpose_producttype_nav', array(
    'type' => 'tab',
    'section' => 'spark_multipurpose_producttype_section',
    'priority' => 1,
    'buttons' => array(
        array(
            'name' => esc_html__('Content', 'xcommerce'),
            'fields' => array(
                'spark_multipurpose_producttype_section_disable',
                'spark_multipurpose_producttype_title',
                'spark_multipurpose_producttype_subtitle',
                'spark_multipurpose_video_calltoaction_alignment',

                'spark_multipurpose_producttype_layout',
                'spark_multipurpose_producttype_category',
                'spark_multipurpose_producttype_category_view',
                'spark_multipurpose_producttype_no_of_product',
                'spark_multipurpose_producttype_no_of_product',
                'spark_multipurpose_producttype_column'
            ),
            'active' => true,
        ),
        array(
            'name' => esc_html__('Style', 'xcommerce'),
            'fields' => array(
                'spark_multipurpose_producttype_cs_heading',
                'spark_multipurpose_producttype_super_title_color',
                'spark_multipurpose_producttype_title_color',
            ),
        ),
        array(
            'name' => esc_html__( 'Advance', 'xcommerce'),
            'fields' => array(
                'spark_multipurpose_producttype_bg_type',
                'spark_multipurpose_producttype_bg_color',
                'spark_multipurpose_producttype_bg_gradient',
                'spark_multipurpose_producttype_bg_image',
                'spark_multipurpose_producttype_bg_video',
                'spark_multipurpose_producttype_overlay_color',

                'spark_multipurpose_producttype_content_heading',
                'spark_multipurpose_producttype_content_bg_type',
                'spark_multipurpose_producttype_content_bg_color',
                'spark_multipurpose_producttype_content_bg_gradient',
                'spark_multipurpose_producttype_content_padding',
                'spark_multipurpose_producttype_content_margin',
                'spark_multipurpose_producttype_content_radius',

                'spark_multipurpose_producttype_padding',
                'spark_multipurpose_producttype_cs_seperator',
                'spark_multipurpose_producttype_seperator0',
                'spark_multipurpose_producttype_section_seperator',
                'spark_multipurpose_producttype_seperator1',
                'spark_multipurpose_producttype_top_seperator',
                'spark_multipurpose_producttype_ts_color',
                'spark_multipurpose_producttype_ts_height',
                'spark_multipurpose_producttype_seperator2',
                'spark_multipurpose_producttype_bottom_seperator',
                'spark_multipurpose_producttype_bs_color',
                'spark_multipurpose_producttype_bs_height'
            )
        ),
        array(
            'name' => esc_html__( 'Hidden', 'xcommerce'),
            'class' => 'hidden customizer-hidden',
            'fields' => array(
                'spark_multipurpose_producttype_text_color',
                'spark_multipurpose_producttype_link_color',
                'spark_multipurpose_producttype_link_hover_color'
            )
        )
    ),
)));


    $wp_customize->add_setting('spark_multipurpose_producttype_title', array(
        'transport' => 'postMessage',
        'sanitize_callback'	=> 'sanitize_text_field'		//done
    ));
    $wp_customize->add_control( 'spark_multipurpose_producttype_title', array(
        'label'	   => esc_html__('Section Title','spark-multipurpose'),
        'section'  => 'spark_multipurpose_producttype_section',
        'type'	   => 'text',
    ));
    
    // Video Call To Action Subtitle.
    $wp_customize->add_setting('spark_multipurpose_producttype_subtitle', array(
        'transport' => 'postMessage',
        'sanitize_callback'	=> 'sanitize_text_field'		//done
    ));
    $wp_customize->add_control('spark_multipurpose_producttype_subtitle', array(
        'label'	   => esc_html__('Section Subtitle','spark-multipurpose'),
        'section'  => 'spark_multipurpose_producttype_section',
        'type'	   => 'text',
    ));

    /** alignment */
    $wp_customize->add_setting('spark_multipurpose_video_calltoaction_alignment',
        array(
            'default'           => 'text-center',
            'sanitize_callback' => 'spark_multipurpose_sanitize_select',
            'transport'         => 'postMessage',
        )
    );
    $wp_customize->add_control(new Spark_Multipurpose_Custom_Control_Buttonset( $wp_customize, 'spark_multipurpose_video_calltoaction_alignment',
        array(
            'choices'  => array(
                'text-left' => esc_html__('Left', 'spark-multipurpose'),
                'text-right' => esc_html__('Right', 'spark-multipurpose'),
                'text-center' => esc_html__('Center', 'spark-multipurpose'),
            ),
            'label'    => esc_html__( 'Alignment', 'spark-multipurpose' ),
            'section'  => 'spark_multipurpose_producttype_section',
            'settings' => 'spark_multipurpose_video_calltoaction_alignment',
        )
    ));
    

$wp_customize->add_setting('spark_multipurpose_producttype_category', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
));
$wp_customize->add_control(new Spark_Multipurpose_Multiple_Check_Control($wp_customize, 'spark_multipurpose_producttype_category', array(
    'section' => 'spark_multipurpose_producttype_section',
    'settings' => 'spark_multipurpose_producttype_category',
    'label' => esc_html__('Select Product Type', 'xcommerce'),
    'choices' => array(
        'latest_product'  => esc_html__('Latest Product', 'xcommerce'),
        'upsell_product'  => esc_html__('UpSell Product', 'xcommerce'),
        'feature_product' => esc_html__('Feature Product', 'xcommerce'),
        'on_sale'         => esc_html__('On Sale Product', 'xcommerce'),
    )
)));
$wp_customize->selective_refresh->add_partial( 'producttype_category', array(
    'settings' => array( 'spark_multipurpose_producttype_category' ),
    'selector' => '#producttype-section', 
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'producttype' );
    }
));
$wp_customize->add_setting('spark_multipurpose_producttype_layout', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'sanitize_text_field',
    'default' => 'tab_styleone',
));
$wp_customize->add_control('spark_multipurpose_producttype_layout', array(
    'section' => 'spark_multipurpose_producttype_section',
    'type' => 'select',
    'label' => esc_html__('Layout', 'xcommerce'),
    'choices' => array(
        'tab_styleone' => __('Style One', 'xcommerce'),
        'tab_styletwo' => __('Style Two', 'xcommerce'),
        'tab_stylethree' => __('Style Three', 'xcommerce'),
    )
));
$wp_customize->add_setting('spark_multipurpose_producttype_category_view', array(
    'default' => 'grid',
    'sanitize_callback' => 'spark_multipurpose_sanitize_select',
    'transport' => 'postMessage'
));
$wp_customize->add_control('spark_multipurpose_producttype_category_view', array(
    'section'   => 'spark_multipurpose_producttype_section',
    'type'      => 'select',
    'label'     => esc_html__('View', 'xcommerce'),
    'choices'   => array(
        'grid'      => esc_html__('Grid', 'xcommerce'),
        'slider'    => esc_html__('Slider', 'xcommerce')
    )
));
$wp_customize->selective_refresh->add_partial( 'producttype_category_view', array(
    'settings' => 'spark_multipurpose_producttype_category_view',
    'selector' => '#producttype-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'producttype' );
    }
));
$wp_customize->add_setting('spark_multipurpose_producttype_column', array(
    'sanitize_callback' => 'absint',
    'default'           => 3,
    'transport' => 'postMessage'
));
$wp_customize->add_control(new Spark_Multipurpose_Range_Control($wp_customize, 'spark_multipurpose_producttype_column', array(
    'section' => 'spark_multipurpose_producttype_section',
    'label' => esc_html__('No of Columns', 'xcommerce'),
    'input_attrs' => array(
        'min' => 2,
        'max' => 4,
        'step' => 1,
    )
)));
$wp_customize->selective_refresh->add_partial( 'producttype_column', array(
    'settings' => array( 'spark_multipurpose_producttype_column' ),
    'selector' => '#cl-producttype-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'producttype' );
    }
));
$wp_customize->add_setting('spark_multipurpose_producttype_no_of_product', array(
    'transport' => 'postMessage',
    'sanitize_callback' => 'absint',
    'default' => 4,
));
$wp_customize->add_control(new Spark_Multipurpose_Range_Control($wp_customize, 'spark_multipurpose_producttype_no_of_product', array(
    'section' => 'spark_multipurpose_producttype_section',
    'label' => esc_html__('No of Products', 'xcommerce'),
    'input_attrs' => array(
        'min' => 4,
        'max' => 12,
        'step' => 1,
    )
)));
$wp_customize->selective_refresh->add_partial( 'producttype_no_of_product', array(
    'settings' => array( 
        'spark_multipurpose_producttype_no_of_product',
        'spark_multipurpose_producttype_section_seperator', 
        'spark_multipurpose_producttype_top_seperator', 
        'spark_multipurpose_producttype_bottom_seperator'
     ),
    'selector' => '#producttype-section',
    'container_inclusive' => true,
    'render_callback' => function() {
        return get_template_part( 'section/section', 'producttype' );
    }
));

$wp_customize->add_setting('spark_multipurpose_producttype_upgrade_text', array(
    'sanitize_callback' => 'sanitize_text_field'
));

$wp_customize->add_control(new Spark_Multipurpose_Upgrade_Text($wp_customize, 'spark_multipurpose_producttype_upgrade_text', array(
    'section' => 'spark_multipurpose_producttype_section',
    'label' => esc_html__('For more settings,', 'xcommerce'),
    'choices' => array(
        esc_html__('Change title styles', 'xcommerce'),
        esc_html__('Different types of layout', 'xcommerce'),
        esc_html__('Change number of columns', 'xcommerce'),
        esc_html__('Switch between Color/Gradient/Image background', 'xcommerce'),
        esc_html__('Advance Option - Directly Change Content from customizer', 'xcommerce'),
    ),
    'priority' => 400
)));