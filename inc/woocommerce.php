<?php
/**
 * WooCommerce Compatibility File
 *
 * @link https://woocommerce.com/
 *
 * @package xCommerce
 */

/**
 * WooCommerce setup function.
 *
 * @link https://docs.woocommerce.com/document/third-party-custom-theme-compatibility/
 * @link https://github.com/woocommerce/woocommerce/wiki/Enabling-product-gallery-features-(zoom,-swipe,-lightbox)
 * @link https://github.com/woocommerce/woocommerce/wiki/Declaring-WooCommerce-support-in-themes
 *
 * @return void
 */
if(!function_exists('xcommerce_setup')){
    function xcommerce_setup() {
        add_theme_support( 'woocommerce' );
        add_theme_support( 'wc-product-gallery-zoom' );
        add_theme_support( 'wc-product-gallery-lightbox' );
        add_theme_support( 'wc-product-gallery-slider' );
    }
}
add_action( 'after_setup_theme', 'xcommerce_setup' );


/**
 * WooCommerce specific scripts & stylesheets.
 *
 * @return void
 */
if( !function_exists('xcommerce_woocommerce_scripts')){
    function xcommerce_woocommerce_scripts() {
        wp_enqueue_style( 'xcommerce-woocommerce-style', get_stylesheet_directory_uri(). '/woocommerce.css', array(), $GLOBALS['xcommerce_version'] );

        $font_path   = WC()->plugin_url() . '/assets/fonts/';
        $inline_font = '@font-face {
                font-family: "star";
                src: url("' . $font_path . 'star.eot");
                src: url("' . $font_path . 'star.eot?#iefix") format("embedded-opentype"),
                    url("' . $font_path . 'star.woff") format("woff"),
                    url("' . $font_path . 'star.ttf") format("truetype"),
                    url("' . $font_path . 'star.svg#star") format("svg");
                font-weight: normal;
                font-style: normal;
            }';

        wp_add_inline_style( 'xcommerce-woocommerce-style', $inline_font );
        wp_enqueue_script( 'countdown', get_stylesheet_directory_uri(). '/js/jquery.countdown.js', array('jquery'), $GLOBALS['xcommerce_version'] );
    }
    add_action( 'wp_enqueue_scripts', 'xcommerce_woocommerce_scripts' );
}

/**
 * Add 'woocommerce-active' class to the body tag.
 *
 * @param  array $classes CSS classes applied to the body tag.
 * @return array $classes modified to include 'woocommerce-active' class.
 */
if( !function_exists('xcommerce_woocommerce_active_body_class')){
    function xcommerce_woocommerce_active_body_class( $classes ) {
        $classes[] = 'woocommerce-active';

        return $classes;
    }
    add_filter( 'body_class', 'xcommerce_woocommerce_active_body_class' );
}

/**
 * Related Products Args.
 *
 * @param array $args related products args.
 * @return array $args related products args.
 */
if(!function_exists('xcommerce_woocommerce_related_products_args')){
    function xcommerce_woocommerce_related_products_args( $args ) {
        $defaults = array(
            'posts_per_page' => 3,
            'columns'        => 3,
        );

        $args = wp_parse_args( $defaults, $args );

        return $args;
    }
    add_filter( 'woocommerce_output_related_products_args', 'xcommerce_woocommerce_related_products_args' );
}

/**
 * Sample implementation of the WooCommerce Mini Cart.
 *
 * You can add the WooCommerce Mini Cart to header.php like so ...
 *
	<?php
		if ( function_exists( 'xcommerce_woocommerce_header_cart' ) ) {
			xcommerce_woocommerce_header_cart();
		}
	?>
 */

if ( ! function_exists( 'xcommerce_woocommerce_cart_link_fragment' ) ) {
	/**
	 * Cart Fragments.
	 *
	 * Ensure cart contents update when products are added to the cart via AJAX.
	 *
	 * @param array $fragments Fragments to refresh via AJAX.
	 * @return array Fragments to refresh via AJAX.
	 */
	function xcommerce_woocommerce_cart_link_fragment( $fragments ) {
		ob_start();
		xcommerce_woocommerce_cart_link();
		$fragments['a.cart-contents'] = ob_get_clean();

		return $fragments;
	}
}
add_filter( 'woocommerce_add_to_cart_fragments', 'xcommerce_woocommerce_cart_link_fragment' );

if ( ! function_exists( 'xcommerce_woocommerce_cart_link' ) ) {
	/**
	 * Cart Link.
	 *
	 * Displayed a link to the cart including the number of items present and the cart total.
	 *
	 * @return void
	 */
	function xcommerce_woocommerce_cart_link() { 
        
        global $woocommerce;

        $icon_or_text = get_theme_mod('cart-icon-options', 'icon');
        $icon         = get_theme_mod('cart-icon', 'fas fa-cart-arrow-down');
        $text         = get_theme_mod('cart-text', 'Cart');
        $show_price   = get_theme_mod('cart-show-price', false);
        $show_count   = get_theme_mod('cart-show-count', true);
        ?>
		<div class="shopcart-dropdown block-cart-link spel-cart">
           <a class="cart-contents" href="<?php echo esc_url( wc_get_cart_url() ); ?>" title="<?php esc_attr_e( 'View your shopping cart', 'xcommerce' ); ?>">
				<div class="site-cart-items-wrap">

                    <?php if( $icon_or_text == 'icon' || $icon_or_text == 'both'): ?>
                    <div class="cart-icon <?php echo esc_attr($icon); ?>">
                        <?php if( $show_count) : ?>
                        <span class="count"><?php echo intval( WC()->cart->cart_contents_count ); ?></span>
                        <?php endif; ?>
                    </div>
                    <?php endif; ?>
                    <?php if( $icon_or_text == 'text' || $icon_or_text == 'both'): ?>
                        <span class="text"><?php echo esc_html($text) ?></span>
                    <?php endif; ?>
                    
                    <?php if($show_price): ?>
                    <span class="item"><?php echo wp_kses_post( $woocommerce->cart->get_cart_subtotal() ); ?></span>
                    <?php endif; ?>
				</div>
            </a>
        </div>
		<?php
	}
}

if ( ! function_exists( 'xcommerce_woocommerce_header_cart' ) ) {
	/**
	 * Display Header Cart.
	 *
	 * @return void
	 */
	function xcommerce_woocommerce_header_cart() {
		if ( is_cart() ) {
			$class = 'current-menu-item';
		} else {
			$class = '';
		}
		?>
		<ul id="site-header-cart" class="site-header-cart">
			<li class="<?php echo esc_attr( $class ); ?>">
				<?php xcommerce_woocommerce_cart_link(); ?>
			</li>
			<li>
				<?php
				$instance = array(
					'title' => '',
				);

				the_widget( 'WC_Widget_Cart', $instance );
				?>
			</li>
		</ul>
		<?php
	}
}
add_action('xcommerce_woocommerce_header_cart', 'xcommerce_woocommerce_header_cart');
/**
 * Sparkle Tabs Category Products Ajax Function
*/
if (!function_exists('xcommerce_tabs_ajax_action')) {

    function xcommerce_tabs_ajax_action() {

        $cat_slug       = $_POST['category_slug'];
        $product_number = $_POST['product_num'];
        $layout         = $_POST['layout']; //'grid';
        $column_number  = $_POST['column']; //3;

        ob_start(); ?>
        <?php
            $product_args = array(
                'post_type' => 'product',
                'tax_query' => array(
                    array(
                        'taxonomy' => 'product_cat',
                        'field' => 'slug',
                        'terms' => $cat_slug
                    )),
                'posts_per_page' => $product_number
            );

            $query = new WP_Query($product_args);

            if ($query->have_posts()) {
                while ($query->have_posts()) { $query->the_post();
                    wc_get_template_part('content', 'product');
                }
            }
            wp_reset_postdata();
        ?>
            
        <?php
            $xcommerce_html = ob_get_contents();
            ob_get_clean();
            echo $xcommerce_html;
            die();
    }
}
add_action('wp_ajax_xcommerce_tabs_ajax_action', 'xcommerce_tabs_ajax_action');
add_action('wp_ajax_nopriv_xcommerce_tabs_ajax_action', 'xcommerce_tabs_ajax_action');

remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_show_product_loop_sale_flash', 10 );
remove_action( 'woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_product_thumbnail', 10 );
if( !function_exists('xcommerce_woocommerce_template_loop_product_thumbnail')){
    function xcommerce_woocommerce_template_loop_product_thumbnail(){ 
        $icon_hover_style = get_theme_mod('xcommerce_woo_product_hover_icon_position', 'left');
        ?>

        <div class="product_wrapper">

            <div class="store_products_item">
                <div class="store_products_item_body">
                    <?php
                        global $post, $product, $product_label_custom; 

                        $sale_class = '';
                        if( $product->is_on_sale() == 1 ){
                            $sale_class = 'new_sale';
                        }
                    ?>
                    <div class="flash <?php echo esc_attr( $sale_class ); ?>">
                        <?php 
                            xcommerce_sale_percentage_loop();
                            if( get_theme_mod('xcommerce_catelog_enable_new_tag', true)):
                                $newness_days = 7;
                                $created = strtotime( $product->get_date_created() );
                                if ( ( time() - ( 60 * 60 * 24 * $newness_days ) ) < $created ) {
                                    xcommerce_flash_sale_new_tag();
                                }
                            endif;
                            

                            if ( $product->is_on_sale() && get_theme_mod('xcommerce_catelog_enable_sales_tag', true) ) :

                                echo apply_filters( 'woocommerce_sale_flash', xcommerce_flash_sale_tag(), $post, $product );
                            
                            endif;
                        ?>
                    </div>

                
                        <?php 
                        if(has_post_thumbnail(  ) ): 
                            the_post_thumbnail('woocommerce_thumbnail'); #Products Thumbnail 
                        else:
                        
                        endif;
                        ?>
                
                </div>
            </div>

        <?php 
    }
}
add_action( 'woocommerce_before_shop_loop_item_title', 'xcommerce_woocommerce_template_loop_product_thumbnail', 10 );

/**
 * Links 
 */
if(!function_exists('xcommerce_add_to_cart_links')){
	function xcommerce_add_to_cart_links(){ ?>
		<div class="store_products_items_info hoverstyletwo">
			<?php if( function_exists( 'xcommerce_quickview' ) || function_exists( 'xcommerce_add_compare_link' ) || function_exists( 'xcommerce_wishlist_products' ) ): ?>
			<div class="xcommerce-buttons-wrapper">
				<?php if(function_exists( 'xcommerce_quickview' )) { ?>
					<div class="products_item_info">
						<?php  xcommerce_quickview(); ?>
					</div>
				<?php } ?>

				<?php if(function_exists( 'xcommerce_add_compare_link' )) { ?>
					<div class="products_item_info">
						<?php  xcommerce_add_compare_link(); ?>
					</div>
				<?php } ?>

				<?php if(function_exists( 'xcommerce_wishlist_products' )) { ?>
					<div class="products_item_info">
						<?php  xcommerce_wishlist_products(); ?>
					</div>
				<?php } ?>
			</div>
			<?php endif; ?>

			<div class="xcommerce-add-to-cart">
				<span class="products_item_info"> 
					<?php
						/**
						 * woocommerce_template_loop_add_to_cart
						*/
						woocommerce_template_loop_add_to_cart();
					?>
				</span>
			</div>

		</div>
		
		<?php
	}
	add_action( 'woocommerce_after_shop_loop_item', 'xcommerce_add_to_cart_links', 10 );
}


/**
 * Percentage calculation function area
*/
if( !function_exists ('xcommerce_sale_percentage_loop') ){
	/**
     * Woocommerce Products Discount Show
     *
     * @since 1.0.0
    */
	function xcommerce_sale_percentage_loop() {

        if( !get_theme_mod('xcommerce_catelog_enable_discount', true) ) return;

		global $product;
		
		if ( $product->is_on_sale() ) {
			
			if ( ! $product->is_type( 'variable' ) and $product->get_regular_price() and $product->get_sale_price() ) {
				
				$max_percentage = ( ( $product->get_regular_price() - $product->get_sale_price() ) / $product->get_regular_price() ) * 100;
			
			} else {
				$max_percentage = 0;
				
				foreach ( $product->get_children() as $child_id ) {

                    $variation = wc_get_product( $child_id );
                    
                    if( !$variation ) continue;

					$price = $variation->get_regular_price();

					$sale = $variation->get_sale_price();

					$percentage = '';

					if ( $price != 0 && ! empty( $sale ) ) $percentage = ( $price - $sale ) / $price * 100;

						if ( $percentage > $max_percentage ) {
							$max_percentage = $percentage;
						}
				}
			
			}
            
            
            

            $color = get_theme_mod('xcommerce_catelog_discount_tag_text_color', '#ffffff');
            $bg_color = get_theme_mod('xcommerce_catelog_discount_tag_bg_color', '#ffc60a');
            
            $style = "style='color: $color; background-color: $bg_color;'";

            echo "<span class='on_sale' ". $style. ">" . esc_html( round( - $max_percentage ) ) . esc_html__("%", 'xcommerce')."</span>";
		
		}

	}
}
/**
 * Add the link to quickview function area
*/
if (defined('YITH_WCQV') && !function_exists('xcommerce_quickview')) {
    function xcommerce_quickview() {
        global $product;
        $quick_view = YITH_WCQV_Frontend();
        remove_action('woocommerce_after_shop_loop_item', array($quick_view, 'yith_add_quick_view_button'), 15);
        $label = esc_html(get_option('yith-wcqv-button-label'));
        echo '<a href="#" class="link-quickview yith-wcqv-button" data-product_id="' . intval( $product->get_id() ) . '">
			<span class="sparkle-tooltip-label">'.esc_html__('Quick view', 'xcommerce').'</span>
			<i class="far fa-eye"></i>
		</a>';
    }
}
/**
 * Add the link to compare function area
*/
if (defined('YITH_WOOCOMPARE') && !function_exists('xcommerce_add_compare_link')) {

    function xcommerce_add_compare_link($product_id = false, $args = array()) {
        extract($args);
        if (!$product_id) {
            global $product;
            $productid = $product->get_id();
            $product_id = isset( $productid ) ? $productid : 0;
        }
        $is_button = !isset($button_or_link) || !$button_or_link ? get_option('yith_woocompare_is_button') : $button_or_link;

        if (!isset($button_text) || $button_text == 'default') {
            $button_text = get_option('yith_woocompare_button_text', esc_html__('Compare', 'xcommerce'));
            yit_wpml_register_string('Plugins', 'plugin_yit_compare_button_text', $button_text);
            $button_text = yit_wpml_string_translate('Plugins', 'plugin_yit_compare_button_text', $button_text);
        }
        printf('<a href="%s" class="%s" data-product_id="%d" rel="nofollow">%s</a>', '#', 'compare link-compare', intval( $product_id ), '
			<span class="sparkle-tooltip-label">'.esc_html( $button_text ).'</span><i class="fas fa-random"></i>' );
    }

    remove_action('woocommerce_after_shop_loop_item', array('YITH_Woocompare_Frontend', 'add_compare_link'), 20);
}
if(!function_exists('xcommerce_flash_sale_tag')){

    function xcommerce_flash_sale_tag(){

        $new_tag_text = get_theme_mod('xcommerce_catelog_enable_sales_tag_text', esc_html__( 'Sale!', 'xcommerce' ));

        $color = get_theme_mod('xcommerce_catelog_enable_sales_tag_text_color', '#ffffff');
        $bg_color = get_theme_mod('xcommerce_catelog_enable_sales_tag_text_bg_color', '#f33c3c');
        
        $style = "style='color: $color; background-color: $bg_color;'";

        return '<span class="store_sale_label" '.$style. ' ><span class="text">' . $new_tag_text . '</span></span>';
    }
}
if(!function_exists('xcommerce_flash_sale_new_tag')){

    function xcommerce_flash_sale_new_tag(){

        $new_tag_text = get_theme_mod('xcommerce_catelog_enable_new_tag_text', esc_html__( 'New!', 'xcommerce' ));

        $color = get_theme_mod('xcommerce_catelog_enable_new_tag_text_color', '#ffffff');
        $bg_color = get_theme_mod('xcommerce_catelog_enable_new_tag_text_bg_color', '#009966');
        
        $style = "style='color: $color; background-color: $bg_color;'";

        echo '<span class="onnew" '.$style. ' ><span class="text">' . $new_tag_text . '</span></span>';
    }
}
/**
 * Product wishlist button function area
*/
if ( function_exists( 'YITH_WCWL' ) && !function_exists('xcommerce_wishlist_products') ) {
    function xcommerce_wishlist_products() {
        echo do_shortcode( '[yith_wcwl_add_to_wishlist]' );
    }
}
/**
 * define the yith-wcwl-browse-wishlist-label callback
*/
if(!function_exists('filter_yith_wcwl_browse_wishlist_label')){
    function filter_yith_wcwl_browse_wishlist_label( $var ) { 

        return '<span class="sparkle-tooltip-label">'.$var.'</span><i class="fas fa-heart"></i>';

    }
}
add_filter( 'yith-wcwl-browse-wishlist-label', 'filter_yith_wcwl_browse_wishlist_label', 10, 1 ); 



/**
 * Result Count & Pagination Wrap
*/
if (!function_exists('xcommerce_woocommerce_before_catalog_ordering')) {

    function xcommerce_woocommerce_before_catalog_ordering(){ ?>

    	<div class="shop-before-control">

      <?php 
    }
}
add_action( 'woocommerce_before_shop_loop', 'xcommerce_woocommerce_before_catalog_ordering', 15);

if (!function_exists('xcommerce_woocommerce_after_catalog_ordering')) {

    function xcommerce_woocommerce_after_catalog_ordering(){ ?>

    	</div>

      <?php 
    }
}
add_action( 'woocommerce_before_shop_loop', 'xcommerce_woocommerce_after_catalog_ordering', 31 );


/********
 * Category List Title
 */
remove_action( 'woocommerce_shop_loop_subcategory_title', 'woocommerce_template_loop_category_title', 10 );
if( !function_exists('xcommerce_template_loop_category_title')){
    function xcommerce_template_loop_category_title( $category ) { 
        ?> 
        <div class="products-cat-info">
            <h3 class="woocommerce-loop-category__title">
                <?php 
                    echo $category->name; 
        
                    if ( $category->count > 0 ) 
                        echo apply_filters( 'woocommerce_subcategory_count_html', ' <span class="count">' . intval($category->count) . ' '.esc_html__('Products','xcommerce').'</span>', $category ); 
                ?> 
            </h3> 
        </div>
        <?php 
    } 
}
add_action( 'woocommerce_shop_loop_subcategory_title', 'xcommerce_template_loop_category_title', 10 );

if( !function_exists('xcommerce_product_tabs_classes')){

	function xcommerce_product_tabs_classes(){
  
		$classes = array('nav','nav-uppercase');
  
		$tab_style = get_theme_mod('xcommerce_woo_product_tab_style','tabs');
		
		if($tab_style == 'tabs' || !$tab_style){
  
			$classes[] = 'nav-line';
  
		} else{
  
			$tab_style = str_replace("tabs_","",$tab_style);
  
			if($tab_style == 'vertical') $classes[] = 'nav-line';
  
			if($tab_style == 'normal') $classes[] = 'nav-tabs';
  
			$classes[] = 'nav-'.$tab_style;
		}
  
		echo implode(' ', $classes);
	}
  }

/******
* Remove Default Title
*/
remove_action( 'woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_title', 10 );
if ( !function_exists('xcommerce_woocommerce_shop_loop_item_title') ) {

    function xcommerce_woocommerce_shop_loop_item_title(){ ?>

        <div class="store_products_item_details">
            <h3>
                <a class="store_products_title" href="<?php the_permalink(); ?>">
                    <?php the_title(); ?>
	          	</a>
          	</h3>
      <?php 
    }
}
add_action( 'woocommerce_shop_loop_item_title', 'xcommerce_woocommerce_shop_loop_item_title', 8 );

/**
 * Price & Rating Wrap
*/
if (!function_exists('xcommerce_woocommerce_before_rating_loop_price')) {

    function xcommerce_woocommerce_before_rating_loop_price(){ ?>

    	<div class="price-rating-wrap"> 

      <?php 
    }
}
add_action( 'woocommerce_after_shop_loop_item_title', 'xcommerce_woocommerce_before_rating_loop_price', 4 );

if (!function_exists('xcommerce_woocommerce_after_rating_loop_price')) {

    function xcommerce_woocommerce_after_rating_loop_price(){  ?>
        
        </div>
            <?php if( is_shop() || is_product_category() || is_product_tag() ) : ?> 
            <?php if( ! get_theme_mod('xcommerce_show_product_description', false) ) return ; ?>
            <div class="xcommerce-more-desc xcommerce-more-effect">
                <div class="woocommerce-product-details__short-description">
                    <?php the_excerpt(); ?>
                </div>
                <a href="#" class="xcommerce-more-desc-btn"><span></span></a>
            </div>

      <?php 
      endif;
    }
}
add_action( 'woocommerce_after_shop_loop_item_title', 'xcommerce_woocommerce_after_rating_loop_price', 12 );



if (!function_exists('xcommerce_woocommerce_product_item_details_close')) {

    function xcommerce_woocommerce_product_item_details_close(){ ?>

    	    </div>
    	</div>

      <?php 
    }
}
add_action( 'woocommerce_after_shop_loop_item', 'xcommerce_woocommerce_product_item_details_close', 12 );


/**
 *  Remove WooCommerce Default Breadcrumb & Title
 */
remove_action('woocommerce_before_main_content', 'woocommerce_breadcrumb', 20);
add_filter('woocommerce_show_page_title', '__return_false');

/**
 * Remove default WooCommerce wrapper.
 */
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10 );
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10 );

if ( ! function_exists( 'sparklestore_woocommerce_wrapper_before' ) ) {
    /**
     * Before Content.
     *
     * Wraps all WooCommerce content in wrappers which match the theme markup.
     *
     * @return void
     */
    function sparklestore_woocommerce_wrapper_before() { 
            if( is_singular('product') ) do_action('spark_multipurpose_breadcrumbs');

            $sidebar = get_theme_mod('spark_multipurpose_catelog_sidebar', 'no-center-content');
            
        ?>

        <div class="container">
            <div class="d-grid d-blog-grid-column-2 sidebar-<?php echo esc_attr($sidebar); ?>">
                <?php if( $sidebar == 'left') get_sidebar('left'); ?>
                <div id="primary" class="content-area page-content">

                    <main id="main" class="site-main" role="main">
                        <div class="articlesListing">
                            <div class="singlearticle">
                                <div class="articlewrap">

                <?php
    }
}
add_action( 'woocommerce_before_main_content', 'sparklestore_woocommerce_wrapper_before' );

if ( ! function_exists( 'sparklestore_woocommerce_wrapper_after' ) ) {
    /**
     * After Content.
     *
     * Closes the wrapping divs.
     *
     * @return void
     */
    function sparklestore_woocommerce_wrapper_after() { 
        $sidebar = get_theme_mod('spark_multipurpose_catelog_sidebar', 'no-center-content');
        ?>
                                </div> <!-- articlewrap -->
                            </div> <!-- .singlearticle -->
                        </div> <!-- articlesListing -->
                    </main>

                </div> <!-- primary -->

                <?php if( $sidebar == 'right') get_sidebar('right'); ?>
            </div>
        </div>
        

        <?php
    }
}
add_action( 'woocommerce_after_main_content', 'sparklestore_woocommerce_wrapper_after' );


/** ===================== Product Page Hooks ================= */
/**
 * Single Product Page Wrapper
*/
if (!function_exists('xcommerce_woocommerce_before_single_product_summary')) {

    function xcommerce_woocommerce_before_single_product_summary(){ 
        ?>
        <div class="product-summary-wrapper clearfix">
      <?php 
    }
}
add_action( 'woocommerce_before_single_product_summary', 'xcommerce_woocommerce_before_single_product_summary', 9);



if (!function_exists('xcommerce_woocommerce_after_single_product_summary')) {

    function xcommerce_woocommerce_after_single_product_summary(){ ?>

    	</div>

      <?php 
    }
}
add_action( 'woocommerce_after_single_product_summary', 'xcommerce_woocommerce_after_single_product_summary', 9 );

/* 
 * Product Single Page
*/
remove_action( 'woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10 );
remove_action( 'woocommerce_before_single_product_summary', 'woocommerce_show_product_sale_flash', 10 );

if( !function_exists('xcommerce_group_flash')){
    function xcommerce_group_flash(){

        global $post, $product; ?>

        <div class="flash">
            <?php 

                xcommerce_sale_percentage_loop(); 

                $newness_days = 7;
                
                if( get_theme_mod('xcommerce_catelog_enable_new_tag', true)):
                    $created = strtotime( $product->get_date_created() );
                    if ( ( time() - ( 60 * 60 * 24 * $newness_days ) ) < $created ) {
                        xcommerce_flash_sale_new_tag();
                    }
                endif;

                if ( $product->is_on_sale() && get_theme_mod('xcommerce_catelog_enable_sales_tag', true ) ) :

                    echo apply_filters( 'woocommerce_sale_flash', xcommerce_flash_sale_tag(), $post, $product );
                
                endif;
            ?>
        </div>

        <?php 
    }
}
add_action( 'woocommerce_single_product_summary','xcommerce_group_flash', 10 );


if( !function_exists('xcommerce_single_disocunt')){
    function xcommerce_single_disocunt(){
        
        if( !get_theme_mod('xcommerce_woo_product_countdown_timer', true)) return;

        global $post, $product; 
        $product_id             = get_the_ID();
        $sale_price_dates_to    = ( $date = get_post_meta( $product_id, '_sale_price_dates_to', true ) ) ? date_i18n( 'Y-m-d', $date ) : '';
        $price_sale             = get_post_meta( $product_id, '_sale_price', true );
        $date                   = date_create($sale_price_dates_to);
        $new_date               = date_format($date,"Y/m/d H:i");
        if(!empty( $sale_price_dates_to ) ) { if(!empty( $price_sale) ) {
    ?>
        <div class="pcountdown-single-page">
            <div class="pcountdown-timer">
                <ul class="countdown_<?php echo intval( $product_id ); ?>">
                    <li><div class="time-days"><span class="days">00</span><span class="time"><?php esc_html_e('Days','xcommerce'); ?></span></div></li>
                    <li><div class="time-hours"><span class="hours">00</span><span class="time"><?php esc_html_e('Hours','xcommerce'); ?></span></div></li>
                    <li><div class="time-minutes"><span class="minutes">00</span><span class="time"><?php esc_html_e('Min','xcommerce'); ?></span></div></li>
                    <li><div class="time-seconds"><span class="seconds">00</span><span class="time"><?php esc_html_e('Sec','xcommerce'); ?></span></div></li>
                </ul>
            </div>
        </div>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                jQuery(".countdown_<?php echo intval( $product_id ); ?>").countdown({
                    date: "<?php echo esc_attr( $new_date ); ?>",
                    offset: -8,
                    day: "Day",
                    days: "Days"
                }, function () {
                //  alert("Done!");
                });
            });
        </script>
    <?php } } 
    }
}

add_action( 'woocommerce_single_product_summary','xcommerce_single_disocunt', 10 );


/*****
 * Comment Overright
 */
if (!function_exists('xcommerce_custom_ratting_single_product')) {
    function xcommerce_custom_ratting_single_product(){
        global $product;
        if ( get_option( 'woocommerce_enable_review_rating' ) === 'no' ) {
            return;
        }
        $rating_count = $product->get_rating_count();
        $average      = $product->get_average_rating();
        if ( $rating_count > 0 ) : ?>
            <div class="woocommerce-product-rating">
                <span class="star-rating">
                    <span style="width:<?php echo( ( intval($average) / 5 ) * 100 ); ?>%">
                        <?php printf(
                            wp_kses( '%1$s out of %2$s', 'xcommerce' ),
                            '<strong class="rating">' . esc_html( $average ) . '</strong>',
                            '<span>5</span>'
                        ); ?>
                    </span>
                </span>

                <span>
                    <?php printf(
                        wp_kses( 'based on %s rating', 'Based on %s ratings', $rating_count, 'xcommerce' ),
                        '<span class="rating">' . esc_html( $rating_count ) . '</span>'
                    ); ?>
                </span>

                <?php if ( comments_open() ) : ?>
                    <a href="#reviews" class="woocommerce-review-link" rel="nofollow">
                        <i class="fas fa-pencil-alt"></i>
                        <?php echo esc_html__( 'write a review', 'xcommerce' ) ?>
                    </a>
                <?php endif ?>
            </div>
        <?php endif;
    }
}
add_action( 'woocommerce_single_product_summary', 'xcommerce_custom_ratting_single_product', 5 );



/**
 * WooCommerce display related product.
*/
if (!function_exists('xcommerce_related_products_args')) {
    function xcommerce_related_products_args( $args ) {
        $args['columns']  = get_theme_mod('xcommerce_woo_single_product_related_column', 3 );
        $args['posts_per_page']  = get_theme_mod('xcommerce_woo_single_product_related_no_of_product', 6 );
        return $args;
    }
}
add_filter( 'woocommerce_output_related_products_args', 'xcommerce_related_products_args' );


if( !function_exists('xcommerce_related_text_strings')){
    // Change Related Products Text
    function xcommerce_related_text_strings( $translated_text, $text, $domain ) {

        switch ( trim($translated_text) ) {

            case 'Related products' :

                $translated_text = get_theme_mod( 'xcommerce_woo_single_product_related_title','Related products' );

                break;
                
            case 'You may also like&hellip;':
                
                $translated_text = get_theme_mod( 'xcommerce_woo_single_product_upsell_title','Upsell products' );

                break;
        }

        return $translated_text;
    }
}
add_filter( 'gettext', 'xcommerce_related_text_strings', 20, 3 );


/**
 * WooCommerce display upsell product.
*/
remove_action( 'woocommerce_after_single_product_summary', 'woocommerce_upsell_display', 15 );
if ( ! function_exists( 'xcommerce_woocommerce_upsell_display' ) ) {
  function xcommerce_woocommerce_upsell_display() {
    if( !get_theme_mod('xcommerce_enable_upsell_product', true)) return;
    
    $posts_per_page   = get_theme_mod( 'xcommerce_woo_single_product_upsell_no_of_product', 6 );
	    
    $upsells_product_columns   = get_theme_mod( 'xcommerce_woo_single_product_upsell_column', 3 );
    
    woocommerce_upsell_display( $posts_per_page, $upsells_product_columns ); 
  }
}
add_action( 'woocommerce_after_single_product_summary', 'xcommerce_woocommerce_upsell_display', 15 );

/**
 * Outputs an extra tab to the default set of info tabs on the single product page.
*/
if( !function_exists('xcommerce_extra_tab_options_tab')){
    function xcommerce_extra_tab_options_tab() { ?>
        <li class="custom_tab"><a href="#custom_tab_data"><?php esc_html_e('Extra Tab Options', 'xcommerce'); ?></a></li>
    <?php
    }
}
add_action('woocommerce_product_write_panel_tabs', 'xcommerce_extra_tab_options_tab'); 


/**
 * Provides the input fields and add/remove buttons for custom tabs on the single product page.
 */
if( !function_exists('xcommerce_tab_options')){
    function xcommerce_tab_options() {
        global $post;   
        $xcommerce_tab_options = array(
            'xcommerce_extra_title'   => get_post_meta( $post->ID, 'xcommerce_extra_tab_title', true ),
            'xcommerce_extra_content' => get_post_meta( $post->ID, 'xcommerce_extra_tab_content', true ),
        );
        
        ?>
        <div id="custom_tab_data" class="panel woocommerce_options_panel">
            <div class="options_group">
                <p class="form-field">
                    <?php 
                        woocommerce_wp_checkbox( array( 'id' => 'xcommerce_tab_enabled', 'label' => esc_html__('Enable Extra Tab ?', 'xcommerce'),
                        'description' => esc_html__('Enable this option to enable the extra tab on the frontend.', 'xcommerce') ) ); 
                    ?>
                </p>
            </div>
            
            <div class="options_group xcommerce_tab_options">                                              
                <p class="form-field">
                    <label><?php esc_html_e('Extra Tab Title :', 'xcommerce'); ?></label>
                    <input class="short" type="text" name="xcommerce_extra_tab_title" value="<?php echo esc_attr( $xcommerce_tab_options['xcommerce_extra_title'] ); ?>" placeholder="<?php esc_attr_e('Enter your extra tab title', 'xcommerce'); ?>" />
                </p>

                <p class="form-field">
                    <label><?php esc_html_e('Extra Tab Content Area :', 'xcommerce'); ?></label>
                </p>
                <p class="extra_tab_options">
                    <?php
                        $settings = array(
                            'text_area_name' => 'xcommerce_extra_tab_content'
                        );
                        $id = 'xcommerce_extra_tab_content';
                        wp_editor( $xcommerce_tab_options['xcommerce_extra_content'], $id, $settings ); 
                    ?>
                </p>
            </div>  
        </div>
    <?php
    }
}
add_action('woocommerce_product_data_panels', 'xcommerce_tab_options');


/**
 * Processes the custom tab options when a post is saved
 */
if( !function_exists('xcommerce_process_product_meta_extra_tab')){
    function xcommerce_process_product_meta_extra_tab( $post_id ) {
        update_post_meta( $post_id, 'xcommerce_tab_enabled', ( isset($_POST['xcommerce_tab_enabled'] ) && $_POST['xcommerce_tab_enabled'] ) ? 'yes' : 'no' );
        update_post_meta( $post_id, 'xcommerce_extra_tab_title', $_POST['xcommerce_extra_tab_title'] );
        update_post_meta( $post_id, 'xcommerce_extra_tab_content', $_POST['xcommerce_extra_tab_content'] );
    }
}
add_action('woocommerce_process_product_meta', 'xcommerce_process_product_meta_extra_tab');

/**
 * extra tab from customizer settigns
 */

add_filter( 'woocommerce_product_tabs', 'xcommerce_woocommerce_product_extra_panel_from_customizer' );
if( !function_exists('xcommerce_woocommerce_product_extra_panel_from_customizer')){
    function xcommerce_woocommerce_product_extra_panel_from_customizer( $tabs ) {
        global $post;       
        $xcommerce_tab_options = array(
            'enabled' => get_theme_mod('xcommerce_woo_show_extra_tab', true),
            'xcommerce_global_title'   => get_theme_mod('xcommerce_woo_product_extra_tab_title')
        );

        if ( $xcommerce_tab_options['enabled'] && $xcommerce_tab_options['xcommerce_global_title']){
            $tabs['global_tab'] = array(
                'title'     => $xcommerce_tab_options['xcommerce_global_title'],
                'priority'  => 50,
                'callback'  => 'xcommerce_woocommerce_product_extra_panel_from_customizer_area'
            );
        }

        return $tabs;

    }
}

if( !function_exists('xcommerce_woocommerce_product_extra_panel_from_customizer_area')){
    function xcommerce_woocommerce_product_extra_panel_from_customizer_area(){
        $xcommerce_tab_options = array(
            'xcommerce_extra_title'   => get_theme_mod('xcommerce_woo_product_extra_tab_title'),
            'xcommerce_extra_content' => get_theme_mod('xcommerce_woo_product_extra_tab'),
        );
        echo force_balance_tags($xcommerce_tab_options['xcommerce_extra_content'] );
    }
}

/** 
 * Add extra tabs to front end product page 
*/
add_filter( 'woocommerce_product_tabs', 'xcommerce_woocommerce_product_extra_panel' );
if( !function_exists('xcommerce_woocommerce_product_extra_panel')){
    function xcommerce_woocommerce_product_extra_panel( $tabs ) {
        global $post;       
        $xcommerce_tab_options = array(
            'enabled' => get_post_meta( $post->ID, 'xcommerce_tab_enabled', true),
            'xcommerce_extra_title'   => get_post_meta($post->ID, 'xcommerce_extra_tab_title', true)
        );

        if ( $xcommerce_tab_options['enabled'] == 'yes' ){
            $tabs['test_tab'] = array(
                'title'     => $xcommerce_tab_options['xcommerce_extra_title'],
                'priority'  => 50,
                'callback'  => 'xcommerce_woocommerce_product_extra_panel_area'
            );
        }

        return $tabs;

    }
}

if( !function_exists('xcommerce_woocommerce_product_extra_panel_area')){
    function xcommerce_woocommerce_product_extra_panel_area() {

        global $post;

        $xcommerce_tab_options = array(
            'xcommerce_extra_title'   => get_post_meta($post->ID, 'xcommerce_extra_tab_title', true),
            'xcommerce_extra_content' => get_post_meta($post->ID, 'xcommerce_extra_tab_content', true),
        );

        echo '<h2>'.esc_html( $xcommerce_tab_options['xcommerce_extra_title'] ).'</h2>';
        echo '<p>'.esc_html( $xcommerce_tab_options['xcommerce_extra_content'] ).'</p>';
        
    }
}

add_action( 'get_header', function () {
    global $product;
    $sidebar = get_theme_mod('spark_multipurpose_catelog_sidebar', 'no-center-content');
    if ( is_product() || (  is_product_category() && $sidebar === 'no-center-content' ) || is_shop() ){
	    remove_action('woocommerce_sidebar', 'woocommerce_get_sidebar', 10 );
    }
});