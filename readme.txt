=== xCommerce ===
Contributors: sparklewpthemes
Tags: one-column, two-columns, right-sidebar, left-sidebar, custom-header, custom-background, custom-menu, translation-ready, featured-images, theme-options, custom-logo, e-commerce, footer-widgets
Requires at least: 5.2
Tested up to: 6.2
Requires PHP: 7.4
Stable tag: 1.0.0
License: GPLv3 or later
License URI: http://www.gnu.org/licenses/gpl-3.0.html

xCommerce is lite lightweight and speed optimized, fully customizable & beautiful WordPress WooCommerce Theme.

== Description ==
xCommerce is lite lightweight and speed optimized, fully customizable & beautiful WordPress WooCommerce Theme. With it sleek and modern design, this theme seamlessly blends aesthetics with functionality, creating a visually engaging experience that keeps your customers coming back for more. For more details visit: https://sparklewpthemes.com/wordpress-themes/xcommerce/


== Installation ==

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Theme supports WooCommerce and some other external plugins like Jetpack, Contact Form 7 and many more plugins.

= Where can I find theme demo? =

You can check our Theme features at https://demo.sptheme.com/xcommerce/

= Where can I find theme all features ? =

You can check our Theme Demo at https://sparklewpthemes.com/wordpress-themes/xcommerce/


== Translation ==

xCommerce theme is translation ready.


== Copyright ==

xCommerce WordPress Theme is child theme of Spark Mulitpurpose, Copyright 2023 Sparkle Themes.
xCommerce is distributed under the terms of the GNU GPL

== Credits ==

	Images used in screenshot

	* https://pxhere.com/en/photo/1024857, License: CCO (https://pxhere.com/en/license)

== Changelog ==
= 1.0.1 10th July 2023 =
 ** Slider Issue Fix
 ** WooCommerce Sidebar issue fix

= 1.0.0 10th July 2023 =
 ** Initial submit theme on wordpress.org trac.